package com.addapptr.infeedbannerdemo;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.addapptr.infeedbannerdemo.models.InfeedBanner;

import java.util.List;

public class AdReloadingRecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private List<Object> listModels;
    private boolean resumed;

    public AdReloadingRecyclerViewScrollListener(List<Object> listModels) {
        this.listModels = listModels;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (resumed) { //handles situations when "onScrolled" is called after activity is paused
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
            int lastVisiblePosition = layoutManager.findLastVisibleItemPosition();

            if (firstVisiblePosition >= 0 && lastVisiblePosition >= 0) {
                int from;
                int to;

                if (dy < 0) { //scrolling up
                    from = Math.max(firstVisiblePosition - 2, 0);
                    to = lastVisiblePosition;
                } else { //scrolling down
                    from = firstVisiblePosition;
                    to = Math.min(lastVisiblePosition + 2, listModels.size());
                }

                for (int i = from; i < to; i++) {
                    if (listModels.get(i) instanceof InfeedBanner) {
                        if (!((InfeedBanner) listModels.get(i)).hasBanner()) {
                            ((InfeedBanner) listModels.get(i)).reloadBanner();
                        }
                    }
                }
            }
        }
        super.onScrolled(recyclerView, dx, dy);
    }

    public void onResume() {
        resumed = true;
    }

    public void onPause() {
        resumed = false;
    }
}
