package com.addapptr.infeedbannerdemo.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.addapptr.infeedbannerdemo.AdReloadingRecyclerViewScrollListener;
import com.addapptr.infeedbannerdemo.InfeedBannerDemoApplication;
import com.addapptr.infeedbannerdemo.R;
import com.addapptr.infeedbannerdemo.models.InfeedBanner;
import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.BannerRequestDelegate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

abstract class ActivityWithAds extends AppCompatActivity implements InfeedBanner.BannerLoadListener {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private AdReloadingRecyclerViewScrollListener scrollListener;
    private List<Object> models = new ArrayList<>();

    void init(RecyclerView.Adapter adapter) {
        this.adapter = adapter;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        scrollListener = new AdReloadingRecyclerViewScrollListener(models);
        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        prepareContentData();
        prepareInfeedBannerData();
        adapter.notifyDataSetChanged();
    }

    abstract void prepareContentData();

    private void prepareInfeedBannerData() {
        for (int position = 0; position < models.size(); position = position + getAdInterval()) {
            InfeedBanner infeedBanner = new InfeedBanner(this, ((InfeedBannerDemoApplication) getApplication()).getInFeedBannerPlacement(), position, getTargetingInformation(), getBannerRequestDelegate());
            models.add(position, infeedBanner);
        }
    }

    protected abstract int getAdInterval();

    protected abstract Map<String,List<String>> getTargetingInformation();

    protected abstract BannerRequestDelegate getBannerRequestDelegate();

    List<Object> getModels() {
        return models;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AATKit.onActivityResume(this);
        scrollListener.onResume();
        scrollListener.onScrolled(recyclerView, 0, 0); //called to trigger ad loading
    }

    @Override
    protected void onPause() {
        scrollListener.onPause();
        for (Object model : models) {
            if (model instanceof InfeedBanner) {
                ((InfeedBanner) model).removeAd(); //remove all ads, they will be reloaded on activity resume
            }
        }
        AATKit.onActivityPause(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        for (Object model : models) {
            if (model instanceof InfeedBanner) {
                ((InfeedBanner) model).onActivityDestroy();
            }
        }
        super.onDestroy();
    }

    @Override
    public void onRequestCompleted(int position) {
        adapter.notifyItemChanged(position);
    }
}
