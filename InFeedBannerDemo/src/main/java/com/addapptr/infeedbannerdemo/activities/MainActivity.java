package com.addapptr.infeedbannerdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.addapptr.infeedbannerdemo.R;
import com.addapptr.infeedbannerdemo.adapters.MainAdapter;
import com.addapptr.infeedbannerdemo.models.MainModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<MainModel> mainModels = new ArrayList<>();
    private MainAdapter mainAdapter;
    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mainAdapter = new MainAdapter(mainModels);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mainAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        gestureDetector = new GestureDetector(MainActivity.this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });

        recyclerView.addOnItemTouchListener(onItemTouchListener());

        prepareMainData();
    }

    private void prepareMainData() {
        MainModel cars = new MainModel("Cars");
        mainModels.add(cars);

        MainModel news = new MainModel("News");
        mainModels.add(news);

        mainAdapter.notifyDataSetChanged();
    }

    private RecyclerView.OnItemTouchListener onItemTouchListener() {
        return new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if (child != null && gestureDetector.onTouchEvent(e)) {
                    Intent intent = null;
                    switch (rv.getChildLayoutPosition(child)) {
                        case 0:
                            intent = new Intent(MainActivity.this, CarActivity.class);
                            break;
                        case 1:
                            intent = new Intent(MainActivity.this, NewsActivity.class);
                            break;
                        default:
                            break;
                    }

                    if (intent != null)
                        startActivity(intent);
                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            }
        };
    }
}
