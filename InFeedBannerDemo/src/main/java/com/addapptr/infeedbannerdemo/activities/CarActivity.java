package com.addapptr.infeedbannerdemo.activities;

import android.os.Bundle;

import com.addapptr.infeedbannerdemo.R;
import com.addapptr.infeedbannerdemo.adapters.CarAdapter;
import com.addapptr.infeedbannerdemo.models.Car;
import com.intentsoftware.addapptr.AdNetwork;
import com.intentsoftware.addapptr.BannerRequest;
import com.intentsoftware.addapptr.BannerRequestDelegate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarActivity extends ActivityWithAds {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);

        init(new CarAdapter(getModels()));
    }

    @Override
    protected Map<String, List<String>> getTargetingInformation() {
        Map<String, List<String>> targetingInformation = new HashMap<>();
        targetingInformation.put("Car", Arrays.asList("Volvo", "BMW", "Audi", "Mazda", "Opel", "Fiat"));
        return targetingInformation;
    }

    @Override
    protected BannerRequestDelegate getBannerRequestDelegate() {
        return new BannerRequestDelegate() {
            @Override
            public boolean shouldUseTargeting(BannerRequest request, AdNetwork network) {
                if (network == AdNetwork.DFP || network == AdNetwork.ADMOB || network == AdNetwork.INMOBI) { //we want to pass targeting information only for selected networks
                    return true;
                } else {
                    return false;
                }
            }
        };
    }

    @Override
    protected int getAdInterval() {
        return 9;
    }

    void prepareContentData() {

        Car car = new Car("Audi", "A4", "2016");
        getModels().add(car);

        car = new Car("Audi", "A8", "2017");
        getModels().add(car);

        car = new Car("Audi", "Q2", "2017");
        getModels().add(car);

        car = new Car("Audi", "Q7", "2017");
        getModels().add(car);

        car = new Car("Audi", "TT", "2017");
        getModels().add(car);

        car = new Car("Audi", "R8", "2017");
        getModels().add(car);

        car = new Car("VW", "Polo", "2017");
        getModels().add(car);

        car = new Car("VW", "Tiguan Allspace", "2017");
        getModels().add(car);

        car = new Car("VW", "Golf", "2017");
        getModels().add(car);

        car = new Car("VW", "Jetta", "2017");
        getModels().add(car);

        car = new Car("VW", "Passat", "2017");
        getModels().add(car);

        car = new Car("VW", "Arteon", "2017");
        getModels().add(car);

        car = new Car("Opel", "Corsa", "2017");
        getModels().add(car);

        car = new Car("Opel", "Karl", "2017");
        getModels().add(car);

        car = new Car("Opel", "Cascada", "2017");
        getModels().add(car);

        car = new Car("Opel", "Astra", "2017");
        getModels().add(car);

        car = new Car("Opel", "Meriva", "2017");
        getModels().add(car);
    }
}
