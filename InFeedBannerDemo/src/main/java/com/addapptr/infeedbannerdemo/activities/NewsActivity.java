package com.addapptr.infeedbannerdemo.activities;

import android.os.Bundle;

import com.addapptr.infeedbannerdemo.R;
import com.addapptr.infeedbannerdemo.adapters.NewsAdapter;
import com.addapptr.infeedbannerdemo.models.News;
import com.intentsoftware.addapptr.BannerRequestDelegate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsActivity extends ActivityWithAds {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        init(new NewsAdapter(getModels()));
    }

    @Override
    protected Map<String, List<String>> getTargetingInformation() {
        Map<String, List<String>> targetingInformation = new HashMap<>();
        targetingInformation.put("Interests", Arrays.asList("Weather", "News", "Movies"));
        return targetingInformation;
    }

    @Override
    protected BannerRequestDelegate getBannerRequestDelegate() {
        return null; //we don't use delegate, so targeting will be passed to all networks
    }

    @Override
    protected int getAdInterval() {
        return 7;
    }

    void prepareContentData() {

        int numberOfArticles = 25;
        for (int i = 1; i < numberOfArticles + 1; i++) {
            News news = new News("Exciting title " + i, "Readlly exciting preface about topic " + i + ".", i + ".10.2017");
            getModels().add(news);
        }
    }
}
