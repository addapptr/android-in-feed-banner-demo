package com.addapptr.infeedbannerdemo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.addapptr.infeedbannerdemo.R;
import com.addapptr.infeedbannerdemo.models.MainModel;
import com.addapptr.infeedbannerdemo.viewholders.MainViewHolder;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainViewHolder> {

    private List<MainModel> mainModels;

    public MainAdapter(List<MainModel> mainModels) {
        this.mainModels = mainModels;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_mainmodel, parent, false);
        return new MainViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        MainModel mainModel = mainModels.get(position);
        holder.title.setText(mainModel.getTitle());
    }

    @Override
    public int getItemCount() {
        return mainModels.size();
    }
}
