package com.addapptr.infeedbannerdemo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.addapptr.infeedbannerdemo.R;
import com.addapptr.infeedbannerdemo.models.News;
import com.addapptr.infeedbannerdemo.viewholders.NewsViewHolder;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends AdapterWithAds {

    private List<Object> newsList = new ArrayList<>();
    private static final int TYPE_NEWS = 1;
    private static final int TYPE_BANNER = 2;

    public NewsAdapter(List<Object> newsList) {
        this.newsList = newsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_news, parent, false);
                return new NewsViewHolder(itemView);
            case 2:
                return getBannerViewHolder(parent);
            default:
        }
        return null;
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (newsList.get(position) instanceof News) {
            News news = (News) newsList.get(position);
            NewsViewHolder newsViewHolder = (NewsViewHolder) holder;
            newsViewHolder.title.setText(news.getTitle());
            newsViewHolder.date.setText(news.getDate());
            newsViewHolder.preface.setText(news.getPreface());
        } else {
            bindBannerViewHolder(holder, newsList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (newsList.get(position) instanceof News) {
            return TYPE_NEWS;
        } else {
            return TYPE_BANNER;
        }
    }
}
