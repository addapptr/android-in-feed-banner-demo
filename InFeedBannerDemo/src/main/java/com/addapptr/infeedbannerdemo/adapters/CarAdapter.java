package com.addapptr.infeedbannerdemo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.addapptr.infeedbannerdemo.R;
import com.addapptr.infeedbannerdemo.models.Car;
import com.addapptr.infeedbannerdemo.viewholders.CarViewHolder;

import java.util.ArrayList;
import java.util.List;

public class CarAdapter extends AdapterWithAds {

    private List<Object> cars = new ArrayList<>();

    private static final int TYPE_CAR = 1;
    private static final int TYPE_BANNER = 2;

    public CarAdapter(List<Object> cars) {
        this.cars = cars;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                View itemViewCar = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_car, parent, false);
                return new CarViewHolder(itemViewCar);
            case 2:
                return getBannerViewHolder(parent);
            default:
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (cars.get(position) instanceof Car) {
            Car car = (Car) cars.get(position);
            CarViewHolder carViewHolder = (CarViewHolder) holder;
            carViewHolder.brand.setText(car.getBrand());
            carViewHolder.model.setText(car.getModel());
            carViewHolder.year.setText(car.getYear());
        } else {
            bindBannerViewHolder(holder, cars.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (cars.get(position) instanceof Car) {
            return TYPE_CAR;
        } else {
            return TYPE_BANNER;
        }
    }
}
