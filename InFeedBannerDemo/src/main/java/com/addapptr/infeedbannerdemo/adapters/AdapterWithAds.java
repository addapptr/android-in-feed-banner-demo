package com.addapptr.infeedbannerdemo.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.addapptr.infeedbannerdemo.R;
import com.addapptr.infeedbannerdemo.models.InfeedBanner;
import com.addapptr.infeedbannerdemo.viewholders.InfeedBannerViewHolder;
import com.intentsoftware.addapptr.BannerPlacementLayout;

abstract class AdapterWithAds extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @NonNull
    RecyclerView.ViewHolder getBannerViewHolder(ViewGroup parent) {
        View itemViewInfeedBanner = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_infeedbanner, parent, false);
        return new InfeedBannerViewHolder(itemViewInfeedBanner);
    }

    void bindBannerViewHolder(RecyclerView.ViewHolder holder, Object banner) {
        InfeedBanner infeedBanner = (InfeedBanner) banner;
        InfeedBannerViewHolder infeedBannerViewHolder = (InfeedBannerViewHolder) holder;

        infeedBannerViewHolder.bannerFrame.removeAllViews();

        BannerPlacementLayout bannerPlacementLayout = infeedBanner.getBannerPlacementLayout();
        if (bannerPlacementLayout != null) {
            if (bannerPlacementLayout.getParent() != null) {
                ((FrameLayout) bannerPlacementLayout.getParent()).removeAllViews();
            }
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);
            infeedBannerViewHolder.bannerFrame.addView(bannerPlacementLayout, params);
        }
    }
}
