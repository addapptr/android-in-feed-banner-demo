package com.addapptr.infeedbannerdemo;

import android.app.Application;

import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.BannerConfiguration;
import com.intentsoftware.addapptr.BannerPlacement;
import com.intentsoftware.addapptr.BannerPlacementLayout;

public class InfeedBannerDemoApplication extends Application {

    private BannerPlacement inFeedBannerPlacement;

    @Override
    public void onCreate() {
        super.onCreate();
        AATKit.init(this, null);

        BannerConfiguration configuration = new BannerConfiguration();
        inFeedBannerPlacement = AATKit.createBannerPlacement("InFeedBanner", configuration);
    }

    public BannerPlacement getInFeedBannerPlacement() {
        return inFeedBannerPlacement;
    }

}
