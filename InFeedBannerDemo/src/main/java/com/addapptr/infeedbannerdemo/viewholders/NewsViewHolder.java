package com.addapptr.infeedbannerdemo.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.addapptr.infeedbannerdemo.R;

public class NewsViewHolder extends RecyclerView.ViewHolder {

    public TextView title;
    public TextView date;
    public TextView preface;

    public NewsViewHolder(View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.title);
        date = itemView.findViewById(R.id.date);
        preface = itemView.findViewById(R.id.preface);
    }
}
