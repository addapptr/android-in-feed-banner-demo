package com.addapptr.infeedbannerdemo.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.addapptr.infeedbannerdemo.R;

public class InfeedBannerViewHolder extends RecyclerView.ViewHolder {

    public FrameLayout bannerFrame;

    public InfeedBannerViewHolder(View itemView) {
        super(itemView);
        bannerFrame = itemView.findViewById(R.id.infeedbanner);
    }
}
