package com.addapptr.infeedbannerdemo.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.addapptr.infeedbannerdemo.R;

public class CarViewHolder extends RecyclerView.ViewHolder {

    public TextView brand;
    public TextView model;
    public TextView year;

    public CarViewHolder(View itemView) {
        super(itemView);
        brand = itemView.findViewById(R.id.brand);
        model = itemView.findViewById(R.id.model);
        year = itemView.findViewById(R.id.year);

    }
}
