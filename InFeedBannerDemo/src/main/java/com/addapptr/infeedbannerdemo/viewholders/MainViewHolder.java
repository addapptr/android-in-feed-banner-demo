package com.addapptr.infeedbannerdemo.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.addapptr.infeedbannerdemo.R;

public class MainViewHolder extends RecyclerView.ViewHolder {

    public TextView title;

    public MainViewHolder(View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.title);
    }
}
