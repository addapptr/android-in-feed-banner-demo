package com.addapptr.infeedbannerdemo.models;

public class News {

    private String title, preface, date;

    public News(String title, String preface, String date) {
        this.title = title;
        this.preface = preface;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public String getPreface() {
        return preface;
    }

    public String getDate() {
        return date;
    }

}
