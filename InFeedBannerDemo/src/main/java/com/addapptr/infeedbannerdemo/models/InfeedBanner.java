package com.addapptr.infeedbannerdemo.models;

import android.util.Log;

import com.intentsoftware.addapptr.BannerPlacement;
import com.intentsoftware.addapptr.BannerPlacementLayout;
import com.intentsoftware.addapptr.BannerRequest;
import com.intentsoftware.addapptr.BannerRequestCompletionListener;
import com.intentsoftware.addapptr.BannerRequestDelegate;
import com.intentsoftware.addapptr.BannerRequestError;

import java.util.List;
import java.util.Map;

public class InfeedBanner implements BannerRequestCompletionListener {

    private BannerPlacementLayout bannerPlacementLayout = null;
    private BannerRequest bannerRequest;

    private BannerLoadListener bannerLoadListener;
    private BannerPlacement placement;
    private int bannerPosition;
    private Map<String, List<String>> targetingInformation;
    private BannerRequestDelegate delegate;

    public interface BannerLoadListener {
        void onRequestCompleted(int position);
    }

    public InfeedBanner(BannerLoadListener bannerLoadListener, BannerPlacement placement, int bannerPosition, Map<String, List<String>> targetingInformation, BannerRequestDelegate delegate) {
        this.bannerLoadListener = bannerLoadListener;
        this.placement = placement;
        this.bannerPosition = bannerPosition;
        this.targetingInformation = targetingInformation;
        this.delegate = delegate;
    }

    public BannerPlacementLayout getBannerPlacementLayout() {
        return bannerPlacementLayout;
    }

    public void reloadBanner() {
        if (bannerRequest == null) {
            bannerRequest = new BannerRequest(delegate);
            bannerRequest.setTargetingInformation(targetingInformation);
            placement.requestAd(bannerRequest, this);
        }
    }

    @Override
    public void onRequestCompleted(BannerPlacementLayout layout, BannerRequestError error) {
        if (layout != null) {
            bannerPlacementLayout = layout;
            bannerLoadListener.onRequestCompleted(bannerPosition);
            bannerRequest = null;
        } else {
            Log.w("InFeedBanner", "Failed to load banner, reason: " + error.getMessage());
        }
    }

    public boolean hasBanner() {
        return (bannerPlacementLayout != null);
    }

    public void removeAd() {
        if (bannerRequest != null) {
            placement.cancel(bannerRequest); //cancel the ad loading
            bannerRequest = null;
        }
        if (bannerPlacementLayout != null) {
            bannerPlacementLayout.destroy(); //destroy the loaded banner
            bannerPlacementLayout = null;
        }
    }

    public void onActivityDestroy() {
        bannerLoadListener = null;
        placement = null;
        targetingInformation = null;
        delegate = null;
    }
}
