package com.addapptr.infeedbannerdemo.models;

public class MainModel {

    private String title;

    public MainModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
